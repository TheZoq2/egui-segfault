use eframe::egui::{self, Align, Layout};

fn main() -> eframe::Result<()> {
    let state = State{};

    let options = eframe::NativeOptions {
        initial_window_size: Some(egui::vec2(320.0, 240.0)),
        ..Default::default()
    };
    eframe::run_native("My egui App", options, Box::new(|_cc| Box::new(state)))
}


struct State {}

impl eframe::App for State {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        println!("Update");
        let max_width = ctx.available_rect().width();

        println!("Drawing left panel");

        egui::SidePanel::left("signal select left panel")
            .default_width(300.)
            .width_range(100.0..=max_width)
            .show(ctx, |ui| {
                ui.vertical(|ui| {
                    egui::Frame::none().show(ui, |ui| {
                        ui.heading("Modules");

                        egui::ScrollArea::both()
                            .show(ui, |ui| {
                                self.draw_all_scopes(ui);
                            });
                    });

                    egui::Frame::none().show(ui, |ui| {
                        ui.heading("Signals");
                    });
                })
            });

        println!("Drawing signal list");

        egui::SidePanel::left("signal list")
            .show(ctx, |ui| {});

        println!("Drawing signals");

        egui::CentralPanel::default().show(ctx, |ui| {});

        println!("Processing messages");
    }
}

impl State {
    pub fn draw_all_scopes(&self, ui: &mut egui::Ui) {
        self.draw_selectable_child_or_orphan_scope(ui);
    }

    fn draw_selectable_child_or_orphan_scope(
        &self,
        ui: &mut egui::Ui,
    ) {
        egui::collapsing_header::CollapsingState::load_with_default_open(
            ui.ctx(),
            egui::Id::new(0),
            false,
        )
        .show_header(ui, |ui| {
            ui.with_layout(
                Layout::top_down(Align::LEFT).with_cross_justify(true),
                |ui| {
                    ui.add(egui::SelectableLabel::new(
                        false, //self.active_scope == Some(scope_idx),
                        "e_proj_cpu_cpu_test_harness",
                    ))
                    // .clicked()
                    // .then(|| msgs.push(Message::HierarchyClick(scope_idx)))
                },
            );
        })
        .body(|ui| /*self.draw_root_scope_view(msgs, vcd, scope_idx, ui)*/());
    }
}
